﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class InputManager : MonoBehaviour
{

    private Vector2 TouchPos;

    private bool IsTouching;


    void Start()
    {
        IsTouching = false;
    }

    void Update()
    {


#if UNITY_EDITOR
        MouseClickController();
#else
        FingerTouchController();
#endif
    }

    private void MouseClickController()
    {

        if (Input.GetMouseButtonDown(0) && !IsTouching)
        {
            TouchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            IsTouching = true;

            RaycastHit2D hit = Physics2D.Raycast(TouchPos, Vector2.zero);

            if (hit.collider != null)
            {
                ClickBehaviour(hit.collider.gameObject);
            }

        }

        else if (Input.GetMouseButtonUp(0) && IsTouching)
        {
            IsTouching = false;

        }

    }

    private void FingerTouchController()
    {

        if (Input.touchCount > 0 && !IsTouching)
        {

            TouchPos = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
            IsTouching = true;

            RaycastHit2D hit = Physics2D.Raycast(TouchPos, Vector2.zero);
            ClickBehaviour(hit.collider.gameObject);

        }
        else if (Input.touchCount == 0 && IsTouching)
        {
            IsTouching = false;
        }

    }


    private void ClickBehaviour(GameObject obj)
    {

        if (obj.tag == "Card" && GameManager.Instance.GameState == GameState.Ongoing)
        {
            obj.GetComponent<CardActor>().OnClick();
        }


    }



}
