using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using DG.Tweening;
using System.Linq;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    public GameState GameState;

    public GameObject CardPrefab;
    public GameObject PlayerPrefab;

    public PlayerActor Player1;
    public PlayerActor Player2;

    public PlayerActor LatestCollector;

    public List<CardActor> FullDeck;
    public List<CardActor> CardPile;

    void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
            return;
        }

        Instance = this;

        GameState = GameState.Paused;

        FullDeck = new List<CardActor>();
        CardPile = new List<CardActor>();

    }

    void Start()
    {
        GenerateCards();
        GeneratePlayers();
        LatestCollector = Player1;

        DOVirtual.DelayedCall(1f, StartGame);
    }

    internal void ResetGame()
    {
        SceneManager.LoadScene("GameScene");
    }

    internal void RoundOver()
    {
        if (FullDeck.Count == 0)
        {
            GameOver();
        }
        else
        {
            DealHand();
        }

    }

    private void GameOver()
    {

        if (CardPile.Count > 0)
        {
            LatestCollector.CollectCardPile();
        }

        if (Player1.CardsCollected.Count > Player2.CardsCollected.Count)
        {
            Player1.Score += 3;
        }
        else if (Player1.CardsCollected.Count < Player2.CardsCollected.Count)
        {
            Player2.Score += 3;
        }

        UiManager.Instance.UpdateScoreTexts();

        UiManager.Instance.ShowEndGamePanel();

    }

    private void GeneratePlayers()
    {
        var player = Instantiate(PlayerPrefab);
        player.GetComponent<PlayerActor>().IsPlaying = true;
        player.name = "Player";
        player.tag = "Player";
        player.transform.position = new Vector2(0f, -3.5f);
        Player1 = player.GetComponent<PlayerActor>();

        var ai = Instantiate(PlayerPrefab);
        ai.GetComponent<PlayerActor>().IsPlaying = false;
        ai.AddComponent<AI>();
        ai.transform.position = new Vector2(0f, 3.5f);
        Player2 = ai.GetComponent<PlayerActor>();
        ai.name = "AI";

    }

    internal int GetCardPileScore()
    {
        return CardPile.Sum(x => x.CardModel.Point);
    }

    internal void PlayCard(CardActor cardActor)
    {

        GameState = GameState.Checking;

        cardActor.Owner.Hand.Remove(cardActor);

        CardPile.Add(cardActor);

        cardActor.SetSortingLayerName("Top");

        if (CardPile.Count > 1)
        {
            CardPile[CardPile.Count - 2].SetSortingLayerName("Bottom");
        }
        if (CardPile.Count > 2)
        {
            CardPile[CardPile.Count - 3].SetSortingLayerName("BottomPack");
        }

        cardActor.FaceUp();
        cardActor.transform.DOMove(new Vector2(-0.2f, 0), 0.2f);



        if (CardPile.Count > 1)
        {
            DOVirtual.DelayedCall(0.5f, CheckPile);
        }
        else
        {
            DOVirtual.DelayedCall(0.5f, ChangeTurnAndResume);
        }


    }

    public void ChangeTurnAndResume()
    {
        ChangeTurn();
        GameState = GameState.Ongoing;
    }

    private void CheckPile()
    {
        CardActor previous = CardPile[CardPile.Count - 2];
        CardActor latest = CardPile[CardPile.Count - 1];

        if (latest.CardModel.IsCollecting(previous.CardModel))
        {
            PlayerActor player = latest.Owner;

            //Pişti
            if (CardPile.Count == 2 && CardPile.FirstOrDefault().CardModel.Rank.Power == 11 && CardPile.LastOrDefault().CardModel.Rank.Power == 11)
            {
                player.Score += 20;
            }
            else if (CardPile.Count == 2 && (CardPile.FirstOrDefault().CardModel.Rank.Power != 11 && CardPile.LastOrDefault().CardModel.Rank.Power != 11))
            {
                player.Score += 10;
            }

            player.CollectCardPile();
        }

        ChangeTurnAndResume();

    }

    private PlayerActor GetLatestPlayer()
    {
        if (Player1.IsPlaying)
        {
            return Player2;
        }
        else
        {
            return Player1;
        }
    }

    private void StartGame()
    {
        for (int i = 0; i < 4; i++)
        {
            CardActor card = GetRandomCardFromDeck();

            FullDeck.Remove(card);
            CardPile.Add(card);

            if (i < 3)
            {
                card.FaceDown();
                card.transform.position = new Vector2(0f, 0f);

                card.SetSortingLayerName("BottomPack");
            }
            else
            {
                card.FaceUp();
                card.transform.position = new Vector2(-0.2f, 0f);

                card.SetSortingLayerName("Top");
            }

            Player1.IsPlaying = true;
            Player2.IsPlaying = false;
        }

        DealHand();

    }

    private void DealHand()
    {
        GameState = GameState.Dealing;

        Sequence seq = DOTween.Sequence();

        int offset1 = 0;
        int offset2 = 0;

        for (int i = 0; i < 8; i++)
        {
            CardActor card = GetRandomCardFromDeck();

            FullDeck.Remove(card);

            if (i % 2 == 0)
            {
                Player1.Hand.Add(card);
                card.Owner = Player1;
                card.SetSortingLayerName((offset1 + 1).ToString());
                card.FaceUp();
                Vector2 movePosition = new Vector2((Player1.transform.position.x - 2f) + offset1, Player1.transform.position.y);
                seq.Append(card.transform.DOMove(movePosition, 0.2f));

                offset1 += 1;
            }
            else
            {
                Player2.Hand.Add(card);
                card.Owner = Player2;
                card.SetSortingLayerName((offset2 + 1).ToString());
                card.FaceDown();
                Vector2 movePosition = new Vector2((Player2.transform.position.x - 2f) + offset2, Player2.transform.position.y);
                seq.Append(card.transform.DOMove(movePosition, 0.2f));

                offset2 += 1;
            }
        }
        DOVirtual.DelayedCall(seq.Duration() + 0.5f, StartRound);
    }

    public void StartRound()
    {
        GameState = GameState.Ongoing;
        Player1.IsPlaying = true;
        Player2.IsPlaying = false;
    }

    private CardActor GetRandomCardFromDeck()
    {
        int randomIndex = UnityEngine.Random.Range(0, FullDeck.Count);
        CardActor card = FullDeck[randomIndex];

        return card;
    }

    private void ChangeTurn()
    {

        Player1.IsPlaying = !Player1.IsPlaying;
        Player2.IsPlaying = !Player2.IsPlaying;

        CheckEndTurn();

    }

    private void CheckEndTurn()
    {

        if (Player1.Hand.Count == 0 && Player2.Hand.Count == 0)
        {
            RoundOver();
        }
    }

    private void GenerateCards()
    {
        var Cards = new GameObject("Cards");

        for (int i = 1; i < 14; i++)
        {
            Suit suit = Suit.Club;

            for (int j = 0; j < 4; j++)
            {
                switch (j)
                {
                    case 0: suit = Suit.Club; break;
                    case 1: suit = Suit.Diamond; break;
                    case 2: suit = Suit.Heart; break;
                    case 3: suit = Suit.Spade; break;
                    default: break;
                }

                Rank rank = new Rank(i);

                Card card;
                if (rank.Power == 11)
                {
                    card = new Jack(rank, suit);
                }
                else
                {
                    card = new Card(rank, suit);
                }

                GameObject cardObject = Instantiate(CardPrefab);
                cardObject.GetComponent<CardActor>().CardModel = card;

                cardObject.transform.position = new Vector2(-6, 0f);
                cardObject.transform.SetParent(Cards.transform);
                cardObject.gameObject.name = cardObject.GetComponent<CardActor>().CardModel.Name;

                FullDeck.Add(cardObject.GetComponent<CardActor>());

            }
        }
    }


}
