﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;

public class UiManager : MonoBehaviour
{
    public static UiManager Instance;

    public GameObject EndGamePanel;
    public Text EndGameText;

    public Text PlayerScoreText;
    public Text AIScoreText;

    void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
            return;
        }

        Instance = this;
    }

    internal void ShowEndGamePanel()
    {
        EndGamePanel.SetActive(true);

        if (GameManager.Instance.Player1.Score > GameManager.Instance.Player2.Score)
        {
            EndGameText.text = "Kazandın";
        }
        else if (GameManager.Instance.Player1.Score < GameManager.Instance.Player2.Score)
        {
            EndGameText.text = "Kaybettin";
        }
        else
        {
            EndGameText.text = "Berabere";
        }

    }

    public void UpdateScoreTexts()
    {
        PlayerScoreText.text = GameManager.Instance.Player1.Score.ToString();
        AIScoreText.text = GameManager.Instance.Player2.Score.ToString();
    }

    public void OnRestartButtonClick()
    {
        GameManager.Instance.ResetGame();
    }
}
