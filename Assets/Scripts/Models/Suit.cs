using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum Suit
{
    Spade,
    Club,
    Diamond,
    Heart
}

