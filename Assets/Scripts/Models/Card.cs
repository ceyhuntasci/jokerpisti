using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Card
{
    public string Name;
    public Rank Rank;
    public Suit Suit;
    public int Point;

    public Card(Rank rank, Suit suit)
    {
        Rank = rank;
        Suit = suit;

        SetPoint();
        SetName();
    }

    public Card()
    {

    }

    public virtual bool IsCollecting(Card target)
    {
        if (Rank.Power == target.Rank.Power)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private void SetName()
    {
        Name = Suit.GetSuitCharacter() + Rank.Power.ToString("00");
    }

    public void SetPoint()
    {
        Point = 0;

        if (Suit == Suit.Club && Rank.Power == 2)
        {
            Point = 2;
        }
        else if (Suit == Suit.Diamond && Rank.Power == 10)
        {
            Point = 3;
        }
        else if (Rank.Power == 11)
        {
            Point = 1;
        }
        else if (Rank.Power == 1)
        {
            Point = 1;
        }
   
    }


}
