

public enum GameState
{
    Dealing,
    Ongoing,
    Paused,
    Checking

}