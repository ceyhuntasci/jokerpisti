using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;

public class AI : MonoBehaviour
{
    public PlayerActor Player;

    void Awake()
    {
        Player = GetComponent<PlayerActor>();
    }

    void Update()
    {
        if (Player.IsPlaying && GameManager.Instance.GameState == GameState.Ongoing)
        {
            DecideAndPlayCard();
        }
    }

    private void DecideAndPlayCard()
    {
        CardActor lastCard = GameManager.Instance.CardPile.LastOrDefault();

        if (lastCard != null)
        {
            CardActor sameRankCard = Player.Hand.Where(x => x.CardModel.Rank.Power == lastCard.CardModel.Rank.Power).FirstOrDefault();

            if (sameRankCard != null)
            {
                GameManager.Instance.PlayCard(sameRankCard);
            }
            else if (DecideToPlayJack())
            {
                GameManager.Instance.PlayCard(Player.Hand.Where(x => x.CardModel.Rank.Power == 11).FirstOrDefault());
            }
            else
            {
                CardActor randomCard = DecideRandomCard();
                GameManager.Instance.PlayCard(randomCard);
            }
        }
        else
        {
            CardActor randomCard = DecideRandomCard();
            GameManager.Instance.PlayCard(randomCard);
        }
    }

    private CardActor DecideRandomCard()
    {
        int sameCardCount = 0;
        CardActor cardToPlay = Player.Hand[0];

        List<CardActor> seenCards = new List<CardActor>();

        seenCards.AddRange(GameManager.Instance.CardPile);
        seenCards.AddRange(GameManager.Instance.Player1.CardsCollected);
        seenCards.AddRange(GameManager.Instance.Player2.CardsCollected);

        for (int i = 0; i < Player.Hand.Count; i++)
        {
            int tempCount = seenCards.Where(x => x.CardModel.Rank.Power == Player.Hand[i].CardModel.Rank.Power).Count();

            if (tempCount >= sameCardCount && Player.Hand[i].CardModel.Rank.Power != 11)
            {
                sameCardCount = tempCount;
                cardToPlay = Player.Hand[i];
            }
        }

        Debug.Log(cardToPlay.CardModel.Name);

        return cardToPlay;
    }

    private bool DecideToPlayJack()
    {
        CardActor jack = Player.Hand.Where(x => x.CardModel.Rank.Power == 11).FirstOrDefault();

        if (jack != null)
        {
            if (GameManager.Instance.GetCardPileScore() > 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
}
