﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class CardActor : MonoBehaviour
{
    SpriteRenderer SpriteRenderer;

    public Sprite CardFrontSprite;
    public Sprite CardBackSprite;
    public Card CardModel;

    public PlayerActor Owner;

    public bool IsFacingUp;

    void Awake()
    {
        SpriteRenderer = GetComponent<SpriteRenderer>();

    }

    void Start()
    {
        LoadCardSprite();
        FaceDown();
    }

    public void FaceDown()
    {
        SpriteRenderer.sprite = CardBackSprite;

        IsFacingUp = false;
    }

    public void FaceUp()
    {
        SpriteRenderer.sprite = CardFrontSprite;

        IsFacingUp = true;
    }

    private void LoadCardSprite()
    {
        CardFrontSprite = Resources.Load<Sprite>("Sprites/Cards/" + CardModel.Name);
    }

    public void OnClick()
    {
        if (Owner != null && Owner.tag == "Player")
        {
            GameManager.Instance.PlayCard(this);
        }
    }

    public void SetSortingLayerName(string name)
    {
        SpriteRenderer.sortingLayerName = name;
    }


}
