using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using DG.Tweening;

public class PlayerActor : MonoBehaviour
{

    public int Score;
    public bool IsPlaying;

    public List<CardActor> Hand;
    public List<CardActor> CardsCollected;

    void Awake()
    {
        Score = 0;
        Hand = new List<CardActor>();
        CardsCollected = new List<CardActor>();
    }

    internal void CollectCardPile()
    {

        for (int i = 0; i < GameManager.Instance.CardPile.Count; i++)
        {
            CardActor card = GameManager.Instance.CardPile[i];

            card.FaceDown();
            CardsCollected.Add(card);
            card.transform.DOMove(new Vector2(transform.position.x + 4, transform.position.y), 0.2f);
            Score += card.CardModel.Point;
        }

        GameManager.Instance.LatestCollector = this;
        GameManager.Instance.CardPile.Clear();

        UiManager.Instance.UpdateScoreTexts();
    }
}