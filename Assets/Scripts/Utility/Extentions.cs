﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class Extentions
{

    public static string GetSuitCharacter(this Suit suit)
    {
        switch (suit)
        {
            case Suit.Spade: return "s";
            case Suit.Diamond: return "d";
            case Suit.Heart: return "h";
            case Suit.Club: return "c";
            default: return "";
        }
    }
}
